// Transmute by Lead Free
// Arduino code for Esp32 Tablet display module
// Writen by Jack Troy using GxEPD library


#include <GxEPD.h>

#include <GxGDEW042Z15/GxGDEW042Z15.cpp>    // 4.2" b/w/r

#include "image_storage.h"

// FreeFonts from Adafruit_GFX
#include <Fonts/FreeMonoBold12pt7b.h>
#include <Fonts/FreeMonoBold18pt7b.h>

#include <GxIO/GxIO_SPI/GxIO_SPI.cpp>
#include <GxIO/GxIO.cpp>

// Global variables
int x,y;
long timeout;
char charString[7];
String xStr, yStr;
GxIO_Class io(SPI, /*CS=5*/ 5, /*DC=*/ 17, /*RST=*/ 16);
GxIO_Class io2(SPI, /*CS=5*/ 19, /*DC=*/ 17, /*RST=*/ 16);
GxEPD_Class display(io, /*RST=*/ 16, /*BUSY=*/ 4);
GxEPD_Class display2(io2, /*RST=*/ 16, /*BUSY=*/ 4);

// Function predef
void drawDot(int x, int y);
void drawTime( void );

void setup() {
  Serial.begin(115200);
  Serial.println("setup");
  // Enable diagnostic output on Serial
  display.init(115200);
  display2.init(115200);

  Serial.println("setup done");
}

void loop() {  
  // If Serial avaliable paint screens
  if (Serial.available()) {
    // Read in 7 byte string "XXX,YYY"
    Serial.readBytes(charString,8);
        // Clear any extra serial
    Serial.flush();
    // C++ String conversion of char array
    yStr = charString;
    // check for "l" precursor (Just to void any wrong serial transmissions)
    if(yStr.substring(0,1)=="l") {
      yStr.remove(0,1);
      // Get position of comma delim
      x = yStr.indexOf(',');
      // xStr = string up until comma
      xStr = yStr.substring(0,x);
      // From yStr remove upto + comma (x+1)
      yStr.remove(0,x+1);
      // int conversion from String
      x = xStr.toInt();
      y = yStr.toInt();
  
      // If 4s has not elapsed then pause till then
      if (millis()<timeout) delay(timeout-millis());
  
      // Draw red dot + map at coords on display 1
      drawDot(x,y);
      // Draw string on display 2
      drawTime();
      
      // Set timeout var to time in 4s
      timeout = millis() + 4000;
    }
  }
}


void drawDot(int x, int y){
  // Init screen
  display.setRotation(1);
  // Draw circle over map image
  display.drawBitmap(map04, 0,0, GxEPD_HEIGHT, GxEPD_WIDTH, GxEPD_BLACK);
  display.drawBitmap(circle, x, y, /*width =*/ 15, /*height = */ 15, GxEPD_RED);
  // Update display
  display.update();
}

void drawTime( void ) {
  // Init screen
  display2.setFont(&FreeMonoBold12pt7b);
  display2.setTextColor(GxEPD_RED);
  display2.setRotation(3);//3
  // Draw text
  display2.setCursor(0,30);
  display2.println("     Next Arrival:");
  display2.println("   Bus 9: 5 minutes");
  display2.setCursor(0,115);
  display2.println("Upcoming services:\n\n55C: 15 minutes\n\n55A: 17 minutes\n\nGKC: 20 minutes");
  // Draw underline
  display2.fillRect( 0, 80, 300, 10, GxEPD_RED);
  // Update display
  display2.update();  
}
