# Camera module

The camera module is designed as a plugin to module 1 (the tablet) and 2 (the bus display).

For both module's the camera has a respective counting mode. Area for tablet, and Flow for display

The module's connect over serial through the two data lines inside of a usb cable. This same cable can be used to supply power.

#### DOCUMENTATION:

The camera module can be initiallised into two mode's Area mode which counts people in an area over a time and Flow mode which counts the flow in and out of people.

##### Initialisation
All initialation and data transfer takes place over serial between the two modules at a baud rate of 9600

Camera --> module : INIT 
> this is sent when the camera boots and signals for the module it is plugged into to send back a mode 

##### Mode Area

Module --> Camera : AREA[TIME]
> example "AREA30"  
> sets the camera into area mode which counts people in area over <TIME> and sends back number at <TIME> increments 

##### Mode Flow

Module --> Camera : FLOW
> sets the camera into flow mode which counts people between START and STOP signals and sends back people in and out

Module --> Camera : START
> starts counting people

Module --> Camera : STOP
> stops counting and sends back data 

Camera --> Module : [IN],[OUT]
> example "30,20"

#### INSTALLTION:
All software for this is run using python3.X on a Raspberry pi zero w. To install on a different SBC you need to change the camera initialation line to point to something other than a rasp-pi camera. 

To run this you need to install the python modules 
> time  
> serial  
> numpy  
> functools  
> random  

(These modules can be installed via pip3)

and opencv (python cv2) which should be installed using instructions for your SBC.

For this project https://www.pyimagesearch.com/2016/04/18/install-guide-raspberry-pi-3-raspbian-jessie-opencv-3/
was used.

Now that everything is installed you can use main.py from this Camera folder, recommend you add it to your start up file.

> git clone https://gitlab.com/iommu/transmute/  
> cd ./transmute/Camera  
> ./main.py  

