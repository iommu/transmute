#!/usr/bin/env python3
##Created by
##Federico Mejia
##Edited by
##Alex Carter

import numpy as np
import cv2
import time
import picamera
import picamera.array

# local modules
import data.Person as Person

#Subtract background
fgbg = cv2.createBackgroundSubtractorMOG2(detectShadows = True)

#Structs for filters
kernelOp = np.ones((3,3),np.uint8)
kernelOp2 = np.ones((5,5),np.uint8)
kernelCl = np.ones((11,11),np.uint8)

#Variables
persons = []
max_p_age = 5
pid = 1

cnt_up   = 0
cnt_down = 0

w = 320
h = 240
frameArea = h*w
areaTH = frameArea/250
print('Area Threshold', areaTH)

#Lineas de entrada/salida
line_up = int(2*(h/5))
line_down   = int(3*(h/5))

up_limit =   int(1*(h/5))
down_limit = int(4*(h/5))

print("Red line y:",str(line_down))
print("Blue line y:", str(line_up))

with picamera.PiCamera() as camera:
    with picamera.array.PiRGBArray(camera) as stream:
        camera.resolution = (320, 240)
        while True:
            camera.capture(stream, 'bgr', use_video_port=True)

            for i in persons:
                i.age_one() #age every person one stream.array
            #########################
            #   PRE-PROCESAMIENTO   #
            #########################

            #Aplica substraccion de fondo
            fgmask = fgbg.apply(stream.array)

            #Binariazcion para eliminar sombras (color gris)
            try:
                ret,imBin= cv2.threshold(fgmask,200,255,cv2.THRESH_BINARY)
                #Opening (erode->dilate) para quitar ruido.
                mask = cv2.morphologyEx(imBin, cv2.MORPH_OPEN, kernelOp)
                #Closing (dilate -> erode) para juntar regiones blancas.
                mask =  cv2.morphologyEx(mask , cv2.MORPH_CLOSE, kernelCl)
            except:
                print("Error!")
                break
            #################
            #   CONTORNOS   #
            #################

            # RETR_EXTERNAL returns only extreme outer flags. All child contours are left behind.
            _, contours0, hierarchy = cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
            for cnt in contours0:
                area = cv2.contourArea(cnt)
                if area > areaTH:
                    #################
                    #   TRACKING    #
                    #################

                    #Falta agregar condiciones para multipersonas, salidas y entradas de pantalla.

                    M = cv2.moments(cnt)
                    cx = int(M['m10']/M['m00'])
                    cy = int(M['m01']/M['m00'])
                    x,y,w,h = cv2.boundingRect(cnt)

                    new = True
                    if cy in range(up_limit,down_limit):
                        for i in persons:
                            if abs(cx-i.getX()) <= w and abs(cy-i.getY()) <= h:
                                # el objeto esta cerca de uno que ya se detecto antes
                                new = False
                                i.updateCoords(cx,cy)   #actualiza coordenadas en el objeto and resets age
                                if i.going_UP(line_down,line_up) == True:
                                    cnt_up += 1;
                                    print("ID:",i.getId(),'crossed going up at',time.strftime("%c")," , ",cnt_up," total.")
                                elif i.going_DOWN(line_down,line_up) == True:
                                    cnt_down += 1;
                                    print("ID:",i.getId(),'crossed going down at',time.strftime("%c")," , ",cnt_down," total.")
                                break
                            if i.getState() == '1':
                                if i.getDir() == 'down' and i.getY() > down_limit:
                                    i.setDone()
                                elif i.getDir() == 'up' and i.getY() < up_limit:
                                    i.setDone()
                            if i.timedOut():
                                #sacar i de la lista persons
                                index = persons.index(i)
                                persons.pop(index)
                                del i     #liberar la memoria de i
                        if new == True:
                            p = Person.MyPerson(pid,cx,cy, max_p_age)
                            persons.append(p)
                            pid += 1
            stream.seek(0)
            stream.truncate()

cap.release()
cv2.destroyAllWindows()
