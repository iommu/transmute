#include <SPI.h>
#include <LoRa.h>

void setup() {
  Serial.begin(9600);
  Serial.println("LoRa Sender");
  LoRa.setPins(8,4,7);
  if (!LoRa.begin(915E6)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
  Serial.println("Ready to send!");
}

void loop() {
  if (Serial.available() > 0 ) {
    String x = Serial.readString();
    LoRa.beginPacket();
    LoRa.print("xyz");
    LoRa.print(x);
    Serial.print("Sending : ");
    Serial.println(x);
    LoRa.endPacket(); 
    Serial.flush();
  }
}
