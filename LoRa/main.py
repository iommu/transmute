# this code was modified by Alex Carter based off of https://github.com/pearsonkyle's code **praise be to pearsonkyle**

from flask import Flask, render_template,request, redirect, url_for
import serial
from serial import SerialException
import time

app = Flask(__name__)

# initialize connection to Arduino (attempt 1411 & 1421, most common)
# change /dev/cu... to the port of your lora send device
connection = 1
try:
    conn = serial.Serial('/dev/cu.usbmodem1411',9600)
    print('LoRa module connected 1411')
except SerialException:
    try:
        conn = serial.Serial('/dev/cu.usbmodem1421',9600)
        print('LoRa module connected 1421')
    except SerialException:
        print('Connection error')
        connection = 0
print("ready")
# we are able to make 2 different requests on our webpage
# GET = loading webpage
# POST = clicking on webpage (actually sends data) 
@app.route('/', methods = ['POST','GET'])
def send():
    # if we make a post request on the webpage aka press button then do stuff
    if request.method == 'POST':
        x = request.form['text']
        print(x)
        if(connection):
            conn.write(x.encode());

    # the default page to display will be our template with our template variables
    return render_template('index.html')


if __name__ == "__main__":

    # lets launch our webpage!
    # do 0.0.0.0 so that we can log into this webpage
    # using another computer on the same network later
    app.run(host='0.0.0.0')
