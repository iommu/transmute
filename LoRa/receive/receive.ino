#include <SPI.h>
#include <LoRa.h>
#include <SoftwareSerial.h>

SoftwareSerial mySerial(9, 5); // RX, TX

void setup() {
  Serial.begin(9600);
  mySerial.begin(115200);
  Serial.println("LoRa Receiver");
  LoRa.setPins(8,4,7);
  if (!LoRa.begin(915E6)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
}

int x,y;
String xStr, yStr;

void loop() {
  // try to parse packet
  int packetSize = LoRa.parsePacket();
  if (packetSize) {
    yStr = LoRa.readString();
    yStr.trim();
    if (yStr.substring(0, 3) == "xyz") {
      yStr.remove(0,3);
      x = yStr.indexOf(',');
      // xStr = string up until comma
      xStr = yStr.substring(0,x);
      // From yStr remove upto + comma (x+1)
      yStr.remove(0,x+1);
      // int conversion from String
      x = xStr.toInt();
      y = yStr.toInt();
      mySerial.print('l');
      if(x<10)
      mySerial.print('0');
      if(x<100) 
      mySerial.print('0');
      mySerial.print(x);
      mySerial.print(',');
      if(y<10)
      mySerial.print('0');
      if(y<100) 
      mySerial.print('0');
      mySerial.print(y);
      Serial.print(x);
      Serial.print(':');
      Serial.println(y);
      
    }
  }
}
