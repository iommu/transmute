import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      home: new SafeArea( 
        child: new Scaffold(
          body: new Stack(
            children: <Widget>[
            new Map(),
            new Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                new SearchBar(),
              ]
            )
          ]),
          bottomNavigationBar: new StopBar(),
        )
      )
    );
  }
}

// Start Map
class Map extends StatefulWidget {
  static const String route = 'map_controller';

  @override
  _MapState createState() {
    return new _MapState();
  }
}
class _MapState extends State<Map> {
  static LatLng bulli = new LatLng(-34.337514, 150.910583);

  MapController mapController;

  void initState() {
    super.initState();
    mapController = new MapController();
  }
  
  Widget build(BuildContext context) {
    var markers = <Marker>[
    new Marker(
      width: 50.0,
      height: 50.0,
      point: new LatLng(-34.337514, 150.910583),
      builder: (ctx) => new Container(
        child: new Icon(Icons.location_on, size:50.0, color:Colors.red),
      ),
    ),
    ];
    return new Scaffold(
      body: new Column(
        children: [
          new Flexible(
            child: new FlutterMap(
              options: new MapOptions(
                center: new LatLng(-34.337514, 150.910583),
                zoom: 18.0,
              ),
              layers: [
                new TileLayerOptions(
                    urlTemplate:
                    "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                    subdomains: ['a', 'b', 'c']),
                new MarkerLayerOptions(markers: markers)
              ],
            ),
          ),
        ],
      ),
    );
  }
}
// End Map

// Start searchbar
class SearchBar extends StatefulWidget {
  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: EdgeInsets.all(20.0),
      child: new Card(
        child: new Padding(
          padding: new EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
          child: new TextField(
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: 'Search locations'
            ),
          )
        )
      )
    );
  }
}
// End searchbar

// Start stopbar
class StopBar extends StatefulWidget {
  @override
  _StopBarState createState() => _StopBarState();
}

class _StopBarState extends State<StopBar> {
  @override
  Widget build(BuildContext context) {
    final List<Widget> rowContents = <Widget> [
      new SizedBox(width: 20.0, height: 60.0,),
      new Expanded( 
        child: new Text(
          'Bulli bus stop',
          style: new TextStyle(fontSize: 18.0)
        ),
        flex:4
      ),
      new Expanded( 
        child: new Row(
        children: <Widget>[
          new Text('30db'),
          new Icon(Icons.bluetooth)
        ],
        ),
        flex:1
      ),
      new Expanded( 
        child: new IconButton(
        icon: const Icon(Icons.location_on),
        onPressed: null
        ),
        flex:1
      )
    ];

    return new BottomAppBar(
      child: new Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        mainAxisSize: MainAxisSize.max,
        children: rowContents
      )
    );
  }
}
// End stopbar